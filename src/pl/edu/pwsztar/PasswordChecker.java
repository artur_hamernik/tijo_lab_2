package pl.edu.pwsztar;

public class PasswordChecker {

    private String password;

    public void savePassword(final String password) {
        this.password = password;
    }

    public void deletePassword() {
        this.password = null;
    }

    public boolean checkPassword(final String password) {
        return this.password != null && this.password.equals(password);
    }
}
