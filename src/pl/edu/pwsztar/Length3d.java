package pl.edu.pwsztar;

public class Length3d {
    public double calculateLengthStructures(final Planet planetA, final Planet planetB){
        return Math.sqrt(Math.pow((planetA.getX() - planetB.getX()),2) + Math.pow((planetA.getY() - planetB.getY()),2) + Math.pow((planetA.getZ() - planetB.getZ()),2));

    }
}
